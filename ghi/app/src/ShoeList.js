import React, {useState, useEffect } from 'react';

function ShoeList() {
    const [shoes, setShoes] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8080/api/shoes/");

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoe)
        }
    }


    useEffect(()=>{
        getData();
    }, [])


    const handleDelete = ((id) => {
        fetch("http://localhost:8080/api/shoes/"+id,{method: "DELETE"}).then(() => {
            window.location.reload()
        })
    });

    return (
        <>
        <table>
          <thead>
            <tr>
              <th>Model</th>
              <th>Manufacturer</th>
              <th>Color</th>
              <th>Bin</th>
            </tr>
          </thead>
          <tbody>
          {shoes.map((shoes) => {
            return (
              <tr key={shoes.id}>
                <td>{ shoes.model_name }</td>
                <td>{ shoes.manufacturer }</td>
                <td>{ shoes.color }</td>
                <td><button onClick={() => handleDelete(shoes.id)}>Delete</button></td>
              </tr>
            );
          })}
          </tbody>
        </table>
        </>
      );
}

export default ShoeList;
