from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import Shoe, BinVO
import json

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "model_name",
        "picture_url",
    ]
    def get_extra_data(self, o):
        return {"bin": o.bin.id}

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "id",
    ]
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "model_name",
        "color",
        "manufacturer",
        "picture_url",
        "bin",
    ]
    encoders= {
        "bin": BinVODetailEncoder()
    }



@require_http_methods(["GET","POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoe = Shoe.objects.all()
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(id=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message: Invalid bin id"},
                status=404,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET","DELETE"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    else:
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
