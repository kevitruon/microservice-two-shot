from django.db import models
from django.urls import reverse

# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    def __str__(self) -> str:
        return self.closet_name


class Shoe(models.Model):
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length = 200)
    manufacturer = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name= "shoes",
        on_delete=models.PROTECT,
    )


    def __str__(self) -> str:
        return self.model_name

    def get_api_url(self):
        return reverse("api_list_shoes", kwargs={"id": self.id})
